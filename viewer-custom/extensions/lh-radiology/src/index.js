
import init from './init.js';

export default {
  /**
   * Only required property. Should be a unique value across all extensions.
   */
  id: 'lh-radiology',

  preRegistration({ configuration = {} }) {
    init({ configuration });
  },
  getToolbarModule() {
    return {
      definitions: [
        {
          id: "FreehandRoi",
          label: "Segment",
          icon: "edit",
          type: "setToolActive",
          commandName: 'setToolActive',
          commandOptions: { toolName: 'FreehandRoi', mouseButtonMask: 1 },
        },
        {
          id: "EraserTool",
          label: "Erazor",
          icon: "trash",
          type: "setToolActive",
          commandName: 'setToolActive',
          commandOptions: { toolName: 'EraserTool', mouseButtonMask: 1 },
        },
        {
          id: "LhSaveTool",
          label: "Save",
          icon: "save",
          type: "setToolActive",
          commandName: 'setToolActive',
          commandOptions: { toolName: 'LhSaveTool', mouseButtonMask: 1 },
        }
      ],
      defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE'
    };
  },
};
