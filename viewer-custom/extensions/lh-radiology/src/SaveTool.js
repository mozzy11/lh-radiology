
import csTools from 'cornerstone-tools';
const BaseTool = csTools.importInternal('base/BaseTool');


export default class SaveTool extends BaseTool {
  constructor(configuration = {}) {
    const defaultConfig = {
      name: 'LhSaveTool',
      supportedInteractionTypes: ['Mouse', 'Touch'],
      configuration: {
        orientation: 0,
      },
    };
    const initialConfiguration = Object.assign(defaultConfig, configuration);

    super(configuration, defaultConfig);
  }

  activeCallback(element) {
   window.globalActiveFunction(element);
  }

  disabledCallback(element) {
   window.globalDisabledFunction(element);
  }

}
