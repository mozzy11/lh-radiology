
import SaveTool from './SaveTool.js';
import cornerstoneTools from 'cornerstone-tools';
/**
 *
 * @param {object} configuration
 * @param {Object|Array} configuration.csToolsConfig
 */
export default function init({ configuration = {} }) {

  const FreehandRoiTool = cornerstoneTools.FreehandRoiTool;

  const EraserTool = cornerstoneTools.EraserTool;

  cornerstoneTools.addTool(FreehandRoiTool, { name: 'FreehandRoi' });

  cornerstoneTools.addTool(EraserTool, { name: 'EraserTool' });

  cornerstoneTools.addTool(SaveTool, { name: 'LhSaveTool' });
}
