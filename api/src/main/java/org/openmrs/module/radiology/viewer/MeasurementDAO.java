/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openmrs.api.db.DAOException;
import org.openmrs.api.db.hibernate.DbSession;
import org.openmrs.api.db.hibernate.DbSessionFactory;
import org.openmrs.module.radiology.viewer.event.MeasurementEventData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("measurementDao")
public class MeasurementDAO {
	
	@Autowired
	DbSessionFactory sessionFactory;
	
	private DbSession getSession() {
		DbSession session = sessionFactory.getCurrentSession();
		return session;
	}
	
	public void setSessionFactory(DbSessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public Measurement saveMeasurement(Measurement measurement) throws DAOException {
		getSession().saveOrUpdate(measurement);
		return measurement;
	}
	
	public Measurement getMeasurementByImage(String studyInstanceUid, String seriesInstanceUid, String sopInstanceUid)
	        throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Measurement.class);
		criteria.add(Restrictions.eq("studyInstanceUid", studyInstanceUid));
		criteria.add(Restrictions.eq("seriesInstanceUid", seriesInstanceUid));
		criteria.add(Restrictions.eq("sopInstanceUid", sopInstanceUid));
		return (Measurement) criteria.uniqueResult();
	}
	
	public void deleteMeasurement(Measurement measurement) throws DAOException {
		getSession().delete(measurement);
	}
	
	public MeasurementEventData saveMeasurementEventData(MeasurementEventData measurementEventData) throws DAOException {
		getSession().saveOrUpdate(measurementEventData);
		return measurementEventData;
	}
	
	@SuppressWarnings("unchecked")
	public List<MeasurementEventData> getAllMeasurementEventData() throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MeasurementEventData.class);
		criteria.addOrder(Order.asc("dateCreated"));
		return criteria.list();
	}
	
	public void deleteMeasurementEventData(MeasurementEventData measurementEventData) throws DAOException {
		getSession().delete(measurementEventData);
	}
}
