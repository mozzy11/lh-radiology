/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.openmrs.Auditable;
import org.openmrs.BaseOpenmrsObject;
import org.openmrs.User;

/**
 * Measurement represents Measurement data per image
 */
@Entity
@Table(name = "radiology_measurement", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "study_uid", "series_uid", "sop_uid" }) })
public class Measurement extends BaseOpenmrsObject implements Auditable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue
    @Column(name = "measurement_id")
    private Integer measurementId;
    
    @Column(name = "study_uid", length = 64, nullable = false)
    private String studyInstanceUid;
    
    @Column(name = "series_uid", length = 64, nullable = false)
    private String seriesInstanceUid;
    
    @Column(name = "sop_uid", length = 64, nullable = false)
    private String sopInstanceUid;
    
    @Access(AccessType.FIELD)
    @ElementCollection
    @CollectionTable(name = "radiology_user_measurement_data", joinColumns = {
            @JoinColumn(name = "measurement_id") }, uniqueConstraints = {
                    @UniqueConstraint(columnNames = { "measurement_id", "user_id" }) })
    @MapKeyJoinColumn(name = "user_id", nullable = false)
    @Column(name = "data", columnDefinition = "mediumtext")
    private Map<User, String> measurementData;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "creator", updatable = false)
    protected User creator;
    
    @Column(name = "date_created", nullable = false, updatable = false)
    private Date dateCreated;
    
    @ManyToOne
    @JoinColumn(name = "changed_by")
    private User changedBy;
    
    @Column(name = "date_changed")
    private Date dateChanged;
    
    /**
     * @see org.openmrs.OpenmrsObject#getId()
     */
    @Override
    public Integer getId() {
        return measurementId;
    }
    
    /**
     * @see org.openmrs.OpenmrsObject#setId(Integer)
     */
    @Override
    public void setId(Integer id) {
        this.measurementId = id;
        
    }
    
    /**
     * Get measurementId of Measurement.
     *
     * @return measurementId of Measurement
     */
    public Integer getMeasurementId() {
        return measurementId;
    }
    
    /**
     * Set measurementId of Measurement.
     *
     * @param measurementId the measurementId id of the Measurement
     */
    public void setMeasurementId(Integer measurementId) {
        this.measurementId = measurementId;
    }
    
    /**
     * Get measurementData as a map of user and actual tool data
     */
    public Map<User, String> getMeasurementData() {
        if (measurementData == null) {
            measurementData = new HashMap<User, String>();
        }
        return measurementData;
    }
    
    /**
     * @see org.openmrs.Auditable#getCreator()
     */
    @Override
    public User getCreator() {
        return creator;
    }
    
    /**
     * @see org.openmrs.Auditable#setCreator(org.openmrs.User)
     */
    @Override
    public void setCreator(User creator) {
        this.creator = creator;
    }
    
    /**
     * @see org.openmrs.Auditable#getDateCreated()
     */
    @Override
    public Date getDateCreated() {
        return dateCreated;
    }
    
    /**
     * @see org.openmrs.Auditable#setDateCreated(java.util.Date)
     */
    @Override
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
    
    /**
     * @see org.openmrs.Auditable#getChangedBy()
     */
    @Override
    public User getChangedBy() {
        return changedBy;
    }
    
    /**
     * @see org.openmrs.Auditable#setChangedBy(org.openmrs.User)
     */
    @Override
    public void setChangedBy(User changedBy) {
        this.changedBy = changedBy;
    }
    
    /**
     * @see org.openmrs.Auditable#getDateChanged()
     */
    @Override
    public Date getDateChanged() {
        return dateChanged;
    }
    
    /**
     * @see org.openmrs.Auditable#setDateChanged(java.util.Date)
     */
    @Override
    public void setDateChanged(Date dateChanged) {
        this.dateChanged = dateChanged;
    }
    
    /**
     * Get Study UID of the image
     */
    public String getStudyInstanceUid() {
        return studyInstanceUid;
    }
    
    /**
     * Set imageId of Measurement.
     *
     * @param studyInstanceUid the studyInstanceUid of the image
     */
    public void setStudyInstanceUid(String studyInstanceUid) {
        this.studyInstanceUid = studyInstanceUid;
    }
    
    /**
     * Get Series UID of the image
     */
    public String getSeriesInstanceUid() {
        return seriesInstanceUid;
    }
    
    /**
     * Set seriesInstanceUid of the Image.
     *
     * @param seriesInstanceUid the seriesInstanceUid of the image
     */
    public void setSeriesInstanceUid(String seriesInstanceUid) {
        this.seriesInstanceUid = seriesInstanceUid;
    }
    
    /**
     * Get SOP UID of the image
     */
    public String getSopInstanceUid() {
        return sopInstanceUid;
    }
    
    /**
     * Set sopInstanceUid of the Image.
     *
     * @param sopInstanceUid the sopInstanceUid of the image
     */
    public void setSopInstanceUid(String sopInstanceUid) {
        this.sopInstanceUid = sopInstanceUid;
    }
}
