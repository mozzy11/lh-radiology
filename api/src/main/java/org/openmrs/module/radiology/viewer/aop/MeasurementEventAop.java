/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer.aop;

import java.lang.reflect.Method;

import org.openmrs.api.context.Context;
import org.openmrs.module.radiology.viewer.Measurement;
import org.openmrs.module.radiology.viewer.MeasurementService;
import org.openmrs.module.radiology.viewer.event.MeasurementEventPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.stereotype.Component;

/**
 * This class implement AOP around the MeasurementService.saveMeasurement in order to fire the
 * Measurement Event
 */
@Component
public class MeasurementEventAop implements AfterReturningAdvice {
    
    private static final Logger log = LoggerFactory.getLogger(MeasurementEventAop.class);
    
    /**
     * Wraps AOP advice Around MeasurementService.saveMeasurement method
     * 
     * @see MeasurementService#saveMeasurement(Measurement)
     */
    @Override
    public void afterReturning(Object returnObject, Method method, Object[] args, Object target) throws Throwable {
        if (method.getName().equals("saveMeasurement")) {
            if (log.isDebugEnabled()) {
                log.debug("Method: {} .After advice called", method.getName());
            }
            MeasurementEventPublisher measurementEventPublisher = Context.getRegisteredComponent("measurementEventPublisher",
                MeasurementEventPublisher.class);
            measurementEventPublisher.publishMeasurementEvent((Measurement) returnObject);
        }
    }
}
