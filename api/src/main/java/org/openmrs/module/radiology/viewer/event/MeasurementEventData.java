/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer.event;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.openmrs.BaseOpenmrsObject;
import org.openmrs.User;

/**
 * MeasurementEventData represents data for Measurement Event
 */
@Entity
@Table(name = "radiology_measurement_event_data")
public class MeasurementEventData extends BaseOpenmrsObject {
    
    @Id
    @GeneratedValue
    @Column(name = "measurement_event_data_id")
    private Integer measurementEventDataId;
    
    @Column(name = "study_uid", length = 64, nullable = false)
    private String studyInstanceUid;
    
    @Column(name = "series_uid", length = 64, nullable = false)
    private String seriesInstanceUid;
    
    @Column(name = "sop_uid", length = 64, nullable = false)
    private String sopInstanceUid;
    
    @Column(name = "data", length = 16777215, nullable = false, columnDefinition = "mediumtext")
    private String data;
    
    @Column(name = "date_created", nullable = false, updatable = false)
    private Date dateCreated;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "creator", updatable = false)
    private User creator;
    
    /**
     * Get Creator
     */
    public User getCreator() {
        return creator;
    }
    
    /**
     * Set Creator.
     *
     * @param creator the user who Created the eventData
     */
    public void setCreator(User creator) {
        this.creator = creator;
    }
    
    /**
     * @see org.openmrs.OpenmrsObject#getId()
     */
    @Override
    public Integer getId() {
        return measurementEventDataId;
    }
    
    /**
     * @see org.openmrs.OpenmrsObject#setId(Integer)
     */
    @Override
    public void setId(Integer measurementEventDataId) {
        this.measurementEventDataId = measurementEventDataId;
        
    }
    
    /**
     * Get Study UID of the image
     */
    public String getStudyInstanceUid() {
        return studyInstanceUid;
    }
    
    /**
     * Set imageId of Measurement.
     *
     * @param studyInstanceUid the studyInstanceUid of the image
     */
    public void setStudyInstanceUid(String studyInstanceUid) {
        this.studyInstanceUid = studyInstanceUid;
    }
    
    /**
     * Get Series UID of the image
     */
    public String getSeriesInstanceUid() {
        return seriesInstanceUid;
    }
    
    /**
     * Set seriesInstanceUid of the Image.
     *
     * @param seriesInstanceUid the seriesInstanceUid of the image
     */
    public void setSeriesInstanceUid(String seriesInstanceUid) {
        this.seriesInstanceUid = seriesInstanceUid;
    }
    
    /**
     * Get SOP UID of the image
     */
    public String getSopInstanceUid() {
        return sopInstanceUid;
    }
    
    /**
     * Set sopInstanceUid of the Image.
     *
     * @param sopInstanceUid the sopInstanceUid of the image
     */
    public void setSopInstanceUid(String sopInstanceUid) {
        this.sopInstanceUid = sopInstanceUid;
    }
    
    /**
     * Get tool measurement Data of the image
     */
    public String getData() {
        return data;
    }
    
    /**
     * Set tool measurement Data of the image.
     *
     * @param data tool data
     */
    public void setData(String data) {
        this.data = data;
    }
    
    /**
     * Get date when MeasurementEventData was created
     */
    public Date getDateCreated() {
        return dateCreated;
    }
    
    /**
     * Set date when MeasurementEventData was created
     *
     * @param date date when MeasurementEventData was created
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
    
}
