/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.DoubleNode;
import org.codehaus.jackson.node.NullNode;
import org.codehaus.jackson.node.ObjectNode;
import org.openmrs.User;
import org.openmrs.api.APIException;

public class MeasurementUtils {
    
    /**
     * merges mesurement tooldata from different json objects into a single json object
     *
     * @param toolDatalist List of the json String to be merged
     * @return JsonNode object of the merged tooldata
     * @throws JsonProcessingException
     * @throws IOException
     */
    public static JsonNode mergeToolData(List<String> toolDataList) {
        
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode mergedToolData = mapper.createObjectNode();
        for (String toolsData : toolDataList) {
            JsonNode jsonToolsData;
            try {
                jsonToolsData = mapper.readTree(toolsData);
            }
            catch (IOException e) {
                throw new APIException(e);
            }
            List<String> toolNamelist = new ArrayList<String>();
            Iterator<String> fieldNames = jsonToolsData.getFieldNames();
            fieldNames.forEachRemaining(toolNamelist::add);
            for (String toolName : toolNamelist) {
                JsonNode toolData = jsonToolsData.get(toolName);
                if (mergedToolData.has(toolName)) {
                    for (JsonNode data : toolData.get("data")) {
                        ((ArrayNode) mergedToolData.get(toolName).get("data")).add(data);
                    }
                    continue;
                }
                mergedToolData.put(toolName, toolData);
            }
        }
        return mergedToolData;
    }
    
    /**
     * Inserts userId into the User Tool Data in case the where UserId is not null
     * 
     * @param data the String data to be encoded
     * @param user the user who owns the data
     * @return String userdata which is encoded with a userId
     * @throws JsonProcessingException
     */
    
    public static String addUserIdIntoUserData(User user, String data) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode updatedUserData;
        try {
            updatedUserData = (ObjectNode) mapper.readTree(data);
        }
        catch (IOException e) {
            throw new APIException(e);
        }
        List<String> toolNamelist = new ArrayList<String>();
        Iterator<String> fieldNames = updatedUserData.getFieldNames();
        fieldNames.forEachRemaining(toolNamelist::add);
        for (String toolName : toolNamelist) {
            JsonNode userToolData = updatedUserData.get(toolName);
            for (JsonNode toolData : userToolData.get("data")) {
                if (!(toolData.has("userId")) || toolData.get("userId") instanceof NullNode) {
                    ((ObjectNode) toolData).put("userId", user.getId());
                }
            }
        }
        return updatedUserData.toString();
    }
    
    /**
     * Reconstructs user data from Admin user merged data
     * 
     * @param mergedAdminData the String data form which user data is to be re-constructed
     * @param user the user who owns the data
     * @return JsonNode of re-constructed user data
     */
    public static JsonNode constructUserData(String mergedAdminData) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode constructedUserData = mapper.createObjectNode();
        
        ObjectNode mergedUsersData;
        try {
            mergedUsersData = (ObjectNode) mapper.readTree(mergedAdminData);
        }
        catch (IOException e) {
            throw new APIException(e);
        }
        List<String> toolNamelist = new ArrayList<String>();
        Iterator<String> fieldNames = mergedUsersData.getFieldNames();
        fieldNames.forEachRemaining(toolNamelist::add);
        for (String toolName : toolNamelist) {
            JsonNode toolData = mergedUsersData.get(toolName);
            for (JsonNode mergedData : toolData.get("data")) {
                if (mergedData.has("userId") && !(mergedData.get("userId") instanceof NullNode)) {
                    String userId = mergedData.get("userId").asText();
                    ArrayNode explodedDataArray;
                    if (constructedUserData.has(userId) && constructedUserData.get(userId).has(toolName)) {
                        explodedDataArray = ((ArrayNode) constructedUserData.get(userId).get(toolName).get("data"));
                        explodedDataArray.add(mergedData);
                    } else {
                        explodedDataArray = mapper.createArrayNode();
                        explodedDataArray.add(mergedData);
                    }
                    
                    ObjectNode explodedData = mapper.createObjectNode();
                    explodedData.put("data", explodedDataArray);
                    
                    ObjectNode explodedToolData;
                    if (constructedUserData.has(userId)) {
                        explodedToolData = (ObjectNode) constructedUserData.get(userId);
                        explodedToolData.put(toolName, explodedData);
                    } else {
                        explodedToolData = mapper.createObjectNode();
                        explodedToolData.put(toolName, explodedData);
                    }
                    constructedUserData.put((userId), explodedToolData);
                }
            }
        }
        return constructedUserData;
    }
    
    /**
     * Generates Bounding Box Cordinates from the RectangleROI tool data
     * 
     * @param toolData the String Json tool data from conerstone
     * @return List of Bounding box co-ordinates
     */
    public static List<Map<String, String>> generateBoundingBoxCordinates(String toolData) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonToolData = mapper.createObjectNode();
        try {
            jsonToolData = (ObjectNode) mapper.readTree(toolData);
        }
        catch (IOException e) {
            throw new APIException(e);
        }
       
        List<Map<String, String>> boxList = new ArrayList<Map<String, String>>();
        JsonNode rectangleToolData = jsonToolData.get("RectangleRoi");
        for (JsonNode data : rectangleToolData.get("data")) {
            DoubleNode x1 = (DoubleNode) data.get("handles").get("start").get("x");
            DoubleNode y1 = (DoubleNode) data.get("handles").get("start").get("y");
            DoubleNode x2 = (DoubleNode) data.get("handles").get("end").get("x");
            DoubleNode y2 = (DoubleNode) data.get("handles").get("end").get("y");
            Map<String, String> box = new HashMap<String, String>();
            //point 1
            box.put("x1", x1.toString());
            box.put("y1", y1.toString());
            //point 2
            box.put("x2", x2.toString());
            box.put("y2", y1.toString());
            //point 3
            box.put("x3", x2.toString());
            box.put("y3", y2.toString());
            //point 4
            box.put("x4", x1.toString());
            box.put("y4", y2.toString());
            boxList.add(box);
        }    
        return boxList;
    }
}
