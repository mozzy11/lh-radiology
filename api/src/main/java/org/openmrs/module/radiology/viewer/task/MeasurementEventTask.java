/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer.task;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.openmrs.api.APIException;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.radiology.viewer.MeasurementService;
import org.openmrs.module.radiology.viewer.event.MeasurementEventData;
import org.openmrs.module.radiology.viewer.utils.MeasurementUtils;
import org.openmrs.scheduler.tasks.AbstractTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A scheduled task that automatically reads MeasurementEventData from the staging table , fetches
 * Image data from the DICOM server and sends Both image and bounding box data to the AI server
 */
public class MeasurementEventTask extends AbstractTask {
    
    private static final Logger log = LoggerFactory.getLogger(MeasurementEventTask.class);
    
    private MeasurementService measurementService;
    
    private AdministrationService administrationService;
    
    @Override
    public void execute() {
        if (log.isDebugEnabled()) {
            log.debug("executing Measurement Event Task");
        }
        measurementService = Context.getService(MeasurementService.class);
        administrationService = Context.getAdministrationService();
        
        List<MeasurementEventData> allEventData = measurementService.getAllMeasurementEventData();
        for (MeasurementEventData eventData : allEventData) {
            try {
                byte[] imageByte = fetchImage(getImageUrl(eventData));
                if (imageByte != null) {
                    for (Map<String, String> map : MeasurementUtils.generateBoundingBoxCordinates(eventData.getData())) {
                        CloseableHttpResponse response = sendDataToAiServer(imageByte, map);
                        int code = response.getStatusLine().getStatusCode();
                        if (code == HttpStatus.SC_OK) {
                            measurementService.purgeMeasurementEventData(eventData);
                        }else{
                            throw new IOException();
                        }
                    }
                }
            }
            catch (IOException e) {
                throw new APIException(e);
            }
        }
        
    }
    
    /**
     * Fetches Image Data from the Dicom server
     * 
     * @param imageUrl the image path from the Dicom server
     * @return byte Array of Image Data
     */
    private byte[] fetchImage(String imageUrl) throws IOException {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpGet get = new HttpGet(imageUrl);
            String username = administrationService.getGlobalProperty("radiology.ohifDicomServerUser");
            String password = administrationService.getGlobalProperty("radiology.ohifDicomServerPassword");
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.UTF_8));
            String authHeader = "Basic " + new String(encodedAuth);
            get.addHeader(HttpHeaders.AUTHORIZATION, authHeader);
            get.addHeader(HttpHeaders.ACCEPT, "image/png");
            try (CloseableHttpResponse resp = httpClient.execute(get)) {
                byte[] imageByte = EntityUtils.toByteArray(resp.getEntity());
                return imageByte;
            }
        }
        
    }
    
    /**
     * Posts data to the AI server
     * 
     * @param imageByte data
     * @param boxCordinates map of constructed Bounding box cordinates
     * @return response
     */
    private CloseableHttpResponse sendDataToAiServer(byte[] imageByte, Map<String, String> boxCordinates)
            throws IOException {
        String aiServerUrl = administrationService.getGlobalProperty("radiology.aiServerBaseUrl") + "/model-update";
        String aiModel = administrationService.getGlobalProperty("radiology.aiModel");
        String aiModelVesrion = administrationService.getGlobalProperty("radiology.aiModelVersion");
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpPost post = new HttpPost(aiServerUrl);
            post.addHeader(HttpHeaders.ACCEPT, "application/json");
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.addBinaryBody("image", imageByte);
            builder.addTextBody("model", aiModel);
            builder.addTextBody("modelVersion", aiModelVesrion);
            builder.addTextBody("x1", boxCordinates.get("x1"));
            builder.addTextBody("x2", boxCordinates.get("x2"));
            builder.addTextBody("x3", boxCordinates.get("x3"));
            builder.addTextBody("x4", boxCordinates.get("x4"));
            builder.addTextBody("y1", boxCordinates.get("y1"));
            builder.addTextBody("y2", boxCordinates.get("y2"));
            builder.addTextBody("y3", boxCordinates.get("y3"));
            builder.addTextBody("y4", boxCordinates.get("y4"));
            HttpEntity multipart = builder.build();
            post.setEntity(multipart);
            try (CloseableHttpResponse resp = httpClient.execute(post)) {
                return resp;
            }
        }
        
    }
    
    /**
     * Constructs image Url to fect the Image from Dicom server
     * 
     * @param eventData the MeasurementEventData
     * @return image Url
     */
    private String getImageUrl(MeasurementEventData eventData) {
        String dicomServerbaseUrl = administrationService.getGlobalProperty("radiology.ohifQidoRoot");
        final String studies = "/studies/";
        final String series = "/series/";
        final String instances = "/instances/";
        final String frame = "/frames/1/rendered";
        StringBuilder sb = new StringBuilder();
        sb.append(dicomServerbaseUrl).append(studies).append(eventData.getStudyInstanceUid()).append(series)
                .append(eventData.getSeriesInstanceUid()).append(instances).append(eventData.getSopInstanceUid())
                .append(frame);
        return sb.toString();
    }
}
