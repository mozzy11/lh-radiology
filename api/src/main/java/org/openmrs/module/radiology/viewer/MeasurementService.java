/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer;

import java.util.List;

import org.openmrs.User;
import org.openmrs.annotation.Authorized;
import org.openmrs.api.APIException;
import org.openmrs.api.OpenmrsService;
import org.openmrs.module.radiology.RadiologyPrivileges;
import org.openmrs.module.radiology.viewer.event.MeasurementEventData;

/**
 * Service layer for {@code Measurement}.
 *
 * @see Measurement
 */
public interface MeasurementService extends OpenmrsService {
        
        /**
         * Get the {@code Measurement} by its {@code imageId}.
         *
         * @param studyInstanceUid studyInstanceUid of the Measurement
         * @param seriesInstanceUid seriesInstanceUid of the Measurement
         * @param sopInstanceUid sopInstanceUid of the Measurement
         * @return Measurement matching given imageId
         * @throws APIException
         */
        @Authorized(RadiologyPrivileges.MANAGE_RADILOGY_MEASUREMENTS)
        Measurement getMeasurementByImage(String studyInstanceUid, String seriesInstanceUid, String sopInstanceUid)
                        throws APIException;
        
        /**
         * Saves a new or updates an existing {@code Measurement}.
         *
         * @param measurement measurement to be saved
         * @return measurement
         * @throws APIException
         */
        @Authorized(RadiologyPrivileges.MANAGE_RADILOGY_MEASUREMENTS)
        Measurement saveMeasurement(Measurement measurement) throws APIException;
        
        /**
         * Get the Json String of user measurement data per Image.
         * 
         * @param studyInstanceUid studyInstanceUid of the Measurement
         * @param seriesInstanceUid seriesInstanceUid of the Measurement
         * @param sopInstanceUid sopInstanceUid of the Measurement
         * @param user user of the Measurement
         * @return Json String of the given user per image
         * @throws APIException
         */
        @Authorized(RadiologyPrivileges.MANAGE_RADILOGY_MEASUREMENTS)
        String getMeasurementDataByImageAndUser(String studyInstanceUid, String seriesInstanceUid, String sopInstanceUid,
                        User user) throws APIException;
        
        /**
         * Completely removes a measurement instance from the database.
         *
         * @param measurement measurementEventData to be purged
         * @throws APIException
         */
        @Authorized(RadiologyPrivileges.MANAGE_RADILOGY_MEASUREMENTS)
        void purgeMeasurement(Measurement measurement) throws APIException;
        
        /**
         * Saves a new or updates an existing {@code MeasurementEventData}.
         *
         * @param measurementEventData measurementEventData to be saved
         * @return measurementEventData
         * @throws APIException
         */
        @Authorized(RadiologyPrivileges.MANAGE_RADILOGY_MEASUREMENTS)
        MeasurementEventData saveMeasurementEventData(MeasurementEventData measurementEventData) throws APIException;
        
        /**
         * Get a List of all MeasurementEventData ordered by date_created
         *
         * @return List of all measurementEventData
         * @throws APIException
         */
        @Authorized(RadiologyPrivileges.MANAGE_RADILOGY_MEASUREMENTS)
        List<MeasurementEventData> getAllMeasurementEventData() throws APIException;
        
        /**
         * Completely remove measurementEventData from the database.
         *
         * @param measurementEventData measurementEventData to be purged
         * @throws APIException
         */
        @Authorized(RadiologyPrivileges.MANAGE_RADILOGY_MEASUREMENTS)
        void purgeMeasurementEventData(MeasurementEventData measurementEventData) throws APIException;
}
