/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer.event;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.openmrs.User;
import org.openmrs.api.context.Daemon;
import org.openmrs.module.DaemonToken;
import org.openmrs.module.radiology.viewer.Measurement;
import org.openmrs.module.radiology.viewer.MeasurementService;
import org.openmrs.module.radiology.viewer.utils.MeasurementUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class MeasurementEventListener implements ApplicationListener<MeasurementApplicationEvent> {
    
    @Autowired
    private MeasurementService measureService;
    
    private static DaemonToken daemonToken;
    
    public static void setDaemonToken(DaemonToken token) {
        daemonToken = token;
    }
    
    /**
     * Listens to the published MeasurementEvent {@code MeasurementEvent} and persists
     * MeasurementEventData
     *
     * @param event MeasurementEvent
     */
    @Override
    public void onApplicationEvent(MeasurementApplicationEvent event) {
        Daemon.runInDaemonThread(new Runnable() {
            
            @Override
            public void run() {
                Measurement measurement = event.getMeasurement();
                MeasurementEventData measurementEventData = new MeasurementEventData();
                measurementEventData.setStudyInstanceUid(measurement.getStudyInstanceUid());
                measurementEventData.setSopInstanceUid(measurement.getSopInstanceUid());
                measurementEventData.setSeriesInstanceUid(measurement.getSeriesInstanceUid());
                List<String> toolDataList = new ArrayList<>();
                for (Map.Entry<User, String> entry : measurement.getMeasurementData().entrySet()) {
                    toolDataList.add(entry.getValue());
                }
                JsonNode toolData = MeasurementUtils.mergeToolData(toolDataList);
                measurementEventData.setData(toolData.toString());
                measureService.saveMeasurementEventData(measurementEventData);
            }
        }, daemonToken);
    }
    
}
