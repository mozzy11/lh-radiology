/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.openmrs.User;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.api.impl.BaseOpenmrsService;
import org.openmrs.module.radiology.RadiologyPrivileges;
import org.openmrs.module.radiology.viewer.event.MeasurementEventData;
import org.openmrs.module.radiology.viewer.utils.MeasurementUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public class MeasurementServiceImpl extends BaseOpenmrsService implements MeasurementService {
    
    @Autowired
    private MeasurementDAO dao;
    
    public void setDao(MeasurementDAO dao) throws APIException {
        this.dao = dao;
    }
    
    /**
     * @see MeasurementService#saveMeasurement(Measurement)
     */
    @Transactional
    @Override
    public Measurement saveMeasurement(Measurement measurement) throws APIException {
        return dao.saveMeasurement(measurement);
    }
    
    /**
     * @see MeasurementService#getMeasurementByImage(String, String, String)
     */
    @Override
    public Measurement getMeasurementByImage(String studyInstanceUid, String seriesInstanceUid, String sopInstanceUid)
            throws APIException {
        return dao.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, sopInstanceUid);
    }
    
    /**
     * @see MeasurementService#getMeasurementDataByImageAndUser(String, String, String, User)
     */
    @Override
    public String getMeasurementDataByImageAndUser(String studyInstanceUid, String seriesInstanceUid, String sopInstanceUid,
            User user) throws APIException {
        
        Measurement measurement = getMeasurementByImage(studyInstanceUid, seriesInstanceUid, sopInstanceUid);
        if (measurement == null) {
            return "{}";
        }
        JsonNode toolData;
        if (Context.hasPrivilege(RadiologyPrivileges.RADIOLOGY_ADMIN)) {
            List<String> toolDataList = new ArrayList<>();
            for (Map.Entry<User, String> entry : measurement.getMeasurementData().entrySet()) {
                toolDataList.add(entry.getValue());
            }
            toolData = MeasurementUtils.mergeToolData(toolDataList);
        } else {
            ObjectMapper mapper = new ObjectMapper();
            try {
                String userData = measurement.getMeasurementData().get(user);
                if (userData == null) {
                    return "{}";
                }
                toolData = mapper.readTree(userData);
            }
            catch (IOException e) {
                throw new APIException(e);
            }
        }
        return toolData.toString();
    }
    
    /**
     * @see MeasurementService#saveMeasurementEventData(MeasurementEventData)
     */
    @Transactional
    @Override
    public MeasurementEventData saveMeasurementEventData(MeasurementEventData measurementEventData) throws APIException {
        return dao.saveMeasurementEventData(measurementEventData);
    }
    
    /**
     * @see MeasurementService#getAllMeasurementEventData()
     */
    @Override
    public List<MeasurementEventData> getAllMeasurementEventData() throws APIException {
        return dao.getAllMeasurementEventData();
    }
    
    /**
     * @see MeasurementService#purgeMeasurementEventData(MeasurementEventData)
     */
    @Transactional
    @Override
    public void purgeMeasurementEventData(MeasurementEventData measurementEventData) throws APIException {
        dao.deleteMeasurementEventData(measurementEventData);
    }
    
    /**
     * @see MeasurementService#purgeMeasurement(Measurement)
     */
    @Transactional
    @Override
    public void purgeMeasurement(Measurement measurement) throws APIException {
        dao.deleteMeasurement(measurement);
    }
}
