/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer;

import java.util.List;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openmrs.User;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.api.context.UserContext;
import org.openmrs.module.TestDaemonToken;
import org.openmrs.module.radiology.RadiologyPrivileges;
import org.openmrs.module.radiology.viewer.event.MeasurementEventData;
import org.openmrs.test.BaseModuleContextSensitiveTest;

public class MeasurementServiceTest extends BaseModuleContextSensitiveTest {
	
	private MeasurementService measureService;
	
	private UserService userService;
	
	private final String studyInstanceUid = "1.2.826.0.1.3680043.8.1055.1.20111103111148288.98361414.79379639";
	
	private final String seriesInstanceUid = "1.2.826.0.1.3680043.8.1055.1.20111103111153403.39691445.89792679";
	
	private final String sopInstanceUid = "1.2.826.0.1.3680043.8.1055.1.20111103111156215.14436053.35031727";
	
	private final String newSopInstanceUid = "1.2.826.0.1.3680043.8.1055.1.20111103111156215.14436053.350317xx";
	
	private User user2;
	
	private final String toolData = "{\"tool-C\":{\"data\":[{\"dataC\":\"xx\"},{\"dataC\":\"xx\"}]}}";
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Before
	public void init() throws Exception {
		executeDataSet("org/openmrs/module/radiology/include/MeasurementTestDataset.xml");
		measureService = Context.getService(MeasurementService.class);
		userService = Context.getUserService();
		user2 = userService.getUser(2);
		new TestDaemonToken().setDaemonToken();
	}
	
	@Test
	public void getMeasurementByImage_shouldGetMeasurementByImage() throws Exception {
		Measurement measurement = measureService.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, sopInstanceUid);
		Assert.assertEquals(measurement.getMeasurementId(), Integer.valueOf(1));
	}

	@Test
	public void purgeMeasurement_shouldDeleteMeasurement() throws Exception {
		Measurement measurement = measureService.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, sopInstanceUid);
		Assert.assertEquals(measurement.getMeasurementId(), Integer.valueOf(1));
		Assert.assertNotNull(measurement);
		measureService.purgeMeasurement(measurement);
		measurement = measureService.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, sopInstanceUid);
		Assert.assertNull(measurement);
	}
	
	@Test
	public void getMeasurementDataByImageAndUser_shouldReturnOnlyUserDataforNonAdmin() throws Exception {
		
		UserContext usercontext = Context.getUserContext();
		usercontext.becomeUser(user2.getSystemId());
		Context.setUserContext(usercontext);
		
		Assert.assertFalse(user2.hasPrivilege(RadiologyPrivileges.RADIOLOGY_ADMIN));
		JsonNode measurementData = mapper.readTree(
		    measureService.getMeasurementDataByImageAndUser(studyInstanceUid, seriesInstanceUid, sopInstanceUid, user2));
		
		Assert.assertTrue(measurementData.has("tool-A"));
		Assert.assertTrue(measurementData.has("tool-X"));
		Assert.assertEquals((measurementData.get("tool-A").get("data")).size(), 2);
		// non Radilology Admin users shouldnt view data for other users
		Assert.assertFalse(measurementData.has("tool-B"));
	}
	
	@Test
	public void getMeasurementDataByImageIdAndUser_shouldReturnAllUserDataforAdmin() throws Exception {
		User user_admin = userService.getUser(4);
		UserContext usercontext = Context.getUserContext();
		usercontext.becomeUser(user_admin.getSystemId());
		Context.setUserContext(usercontext);
		
		JsonNode measurementData = mapper.readTree(measureService.getMeasurementDataByImageAndUser(studyInstanceUid,
		    seriesInstanceUid, sopInstanceUid, user_admin));
		// Radilology Admin users shouldnt view data for other users
		Assert.assertTrue(measurementData.has("tool-A"));
		Assert.assertTrue(measurementData.has("tool-X"));
		Assert.assertEquals((measurementData.get("tool-A").get("data")).size(), 2);
		Assert.assertTrue(measurementData.has("tool-B"));
	}
	
	@Test
	public void saveMeasurement_shouldCreateNewMeasurement() throws Exception {
		Measurement measurement = new Measurement();
		measurement.setStudyInstanceUid(studyInstanceUid);
		measurement.setSeriesInstanceUid(seriesInstanceUid);
		measurement.setSopInstanceUid(newSopInstanceUid);
		User user5 = userService.getUser(5);
		measurement.getMeasurementData().put(user5, toolData);
		Assert.assertNull(measureService.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, newSopInstanceUid));
		Measurement newMeasurement = measureService.saveMeasurement(measurement);
		// after saving
		Assert.assertNotNull(measureService.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, newSopInstanceUid));
		Assert.assertEquals(newMeasurement.getMeasurementData().get(user5), toolData);
	}
	
	@Test
	public void saveMeasurementEventData_shouldSaveMeasurementEventData() throws Exception {
		MeasurementEventData measurementEventData = new MeasurementEventData();
		measurementEventData.setSeriesInstanceUid(seriesInstanceUid);
		measurementEventData.setSopInstanceUid(sopInstanceUid);
		measurementEventData.setStudyInstanceUid(studyInstanceUid);
		measurementEventData.setData(toolData);
		// before Save
		Assert.assertNull(measurementEventData.getId());
		// after Save
		MeasurementEventData newMeasurementEventData = measureService.saveMeasurementEventData(measurementEventData);
		Assert.assertNotNull(newMeasurementEventData.getId());
		Assert.assertEquals(newMeasurementEventData.getSeriesInstanceUid(), seriesInstanceUid);
		Assert.assertEquals(newMeasurementEventData.getSopInstanceUid(), sopInstanceUid);
		Assert.assertEquals(newMeasurementEventData.getStudyInstanceUid(), studyInstanceUid);
	}
	
	@Test
	public void getAllMeasurementEventData_shouldReturnAllMeasurementEventDataInOrderOfDateCreated() throws Exception {
		List<MeasurementEventData> allEventData = measureService.getAllMeasurementEventData();
		Assert.assertTrue(allEventData.get(0).getDateCreated().compareTo(allEventData.get(1).getDateCreated()) < 0);
	}
	
	@Test
	public void purgeMeasurementEventData_shouldPurgeMeasurementEventData() throws Exception {
		List<MeasurementEventData> allEventData = measureService.getAllMeasurementEventData();
		measureService.purgeMeasurementEventData(allEventData.get(0));
		Assert.assertEquals(measureService.getAllMeasurementEventData().size(), allEventData.size() - 1);
	}
}
