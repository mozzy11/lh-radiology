/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.openmrs.User;
import org.openmrs.module.radiology.viewer.utils.MeasurementUtils;

@RunWith(MockitoJUnitRunner.class)
public class MeasurementUtilsTest {
        
        @Mock
        private User user;
        
        @Test
        public void mergeToolData_shouldMergeDifferentToolDataIntoOneJsonObject()
                        throws JsonProcessingException, IOException {
                String dataA = "{\"tool-A\":{\"data\":[{\"dataA\":\"xx\"},{\"dataA\":\"xx\"}]},\"tool-X\":{\"data\":[{\"dataA\":\"xx\"},{\"dataA\":\"xx\"}]}}";
                String dataA2 = "{\"tool-A\":{\"data\":[{\"dataA2\":\"xx\"},{\"dataA2\":\"xy\"}]}}";
                String dataB = "{\"tool-B\":{\"data\":[{\"dataB\":\"xx\"},{\"dataB\":\"xz\"}]}}";
                String dataB2 = "{\"tool-B\":{\"data\":[{\"dataB2\":\"xx\"},{\"dataB2\":\"xv\"}]}}";
                String dataC = "{\"tool-C\":{\"data\":[{\"dataC\":\"xx\"},{\"dataC\":\"xw\"}]}}";
                
                List<String> toolDataList = new ArrayList<>();
                toolDataList.add(dataA);
                toolDataList.add(dataA2);
                toolDataList.add(dataB);
                toolDataList.add(dataB2);
                toolDataList.add(dataC);
                
                JsonNode mergedToolData = MeasurementUtils.mergeToolData(toolDataList);
                
                Assert.assertTrue(mergedToolData.has("tool-X"));
                Assert.assertTrue((mergedToolData.get("tool-X").get("data")).isArray());
                Assert.assertEquals((mergedToolData.get("tool-X").get("data")).size(), 2);
                Assert.assertTrue(mergedToolData.has("tool-A"));
                Assert.assertTrue((mergedToolData.get("tool-A").get("data")).isArray());
                Assert.assertEquals((mergedToolData.get("tool-A").get("data")).size(), 4);
                Assert.assertTrue(mergedToolData.has("tool-B"));
                Assert.assertTrue((mergedToolData.get("tool-B").get("data")).isArray());
                Assert.assertEquals((mergedToolData.get("tool-B").get("data")).size(), 4);
                Assert.assertTrue(mergedToolData.has("tool-C"));
                Assert.assertTrue((mergedToolData.get("tool-C").get("data")).isArray());
                Assert.assertEquals((mergedToolData.get("tool-C").get("data")).size(), 2);
        }
        
        @Test
        public void addUserIdIntoUserData_shouldInsertUserIdIntoUserMeasuremetData()
                        throws JsonProcessingException, IOException {
                when(user.getId()).thenReturn(1);
                final String plainData = "{\"tool-C\":{\"data\":[{\"dataC\":\"xx1\",\"userId\": 5},{\"dataC\":\"xx2\" ,\"userId\": null}]}}";
                ObjectMapper mapper = new ObjectMapper();
                JsonNode innitialJsonData = mapper.readTree(plainData);
                
                // should only Update where user Id is null
                String innitialUserId = ((ArrayNode) innitialJsonData.get("tool-C").get("data")).get(1).get("userId")
                                .asText();
                Assert.assertEquals(innitialUserId, "null");
                
                String updatedData = MeasurementUtils.addUserIdIntoUserData(user, plainData);
                JsonNode updatedJsonData = mapper.readTree(updatedData);
                String updateUserId = ((ArrayNode) updatedJsonData.get("tool-C").get("data")).get(1).get("userId").asText();
                Assert.assertEquals(updateUserId, user.getId().toString());
                
                // shoud not update the userId where theres a value
                String innitialUserId1 = ((ArrayNode) innitialJsonData.get("tool-C").get("data")).get(0).get("userId")
                                .asText();
                Assert.assertEquals(innitialUserId1, "5");
                
                String updatedData1 = MeasurementUtils.addUserIdIntoUserData(user, plainData);
                JsonNode updatedJsonData1 = mapper.readTree(updatedData1);
                String updateUserId1 = ((ArrayNode) updatedJsonData1.get("tool-C").get("data")).get(0).get("userId")
                                .asText();
                Assert.assertEquals(updateUserId1, "5");
        }
        
        @Test
        public void constructUserData_shouldReconstructUserDataFromMergedData() throws IOException {
                
                final String mergedData = "{\"tool-C\":{\"data\":[{\"dataC\":\"xxg\",\"userId\": 5},{\"dataC\":\"xx1\",\"userId\": 5},{\"dataD\":\"xxD\",\"userId\": 5},{\"dataC\":\"xx2\" ,\"userId\": 3}]},\"tool-E\":{\"data\":[{\"dataC\":\"xx_d\",\"userId\": 5},{\"dataC\":\"xx2\",\"userId\": 5},{\"dataC\":\"xx2\" ,\"userId\": 3}]} ,\"tool-Y\":{\"data\":[{\"dataC\":\"xx1\",\"userId\": 5},{\"dataC\":\"xx2\" ,\"userId\": 4}]}}";
                
                JsonNode reconstructedUserData = MeasurementUtils.constructUserData(mergedData);
                Assert.assertTrue(reconstructedUserData.has("3"));
                Assert.assertTrue(reconstructedUserData.has("4"));
                
                // testing json Node for User 5 re-constructed Data
                Assert.assertTrue(reconstructedUserData.has("5"));
                Assert.assertTrue(reconstructedUserData.get("5").has("tool-C"));
                Assert.assertTrue(reconstructedUserData.get("5").has("tool-E"));
                Assert.assertTrue(reconstructedUserData.get("5").has("tool-Y"));
                Assert.assertEquals(((ArrayNode) reconstructedUserData.get("5").get("tool-C").get("data")).size(), 3);
                Assert.assertEquals(((ArrayNode) reconstructedUserData.get("5").get("tool-E").get("data")).size(), 2);
                Assert.assertEquals(((ArrayNode) reconstructedUserData.get("5").get("tool-Y").get("data")).size(), 1);
                
                Assert.assertEquals(
                        (reconstructedUserData.get("5").get("tool-C").get("data").get(0).get("userId").getIntValue()), 5);
                Assert.assertEquals(
                        (reconstructedUserData.get("5").get("tool-C").get("data").get(1).get("userId").getIntValue()), 5);
                Assert.assertEquals(
                        (reconstructedUserData.get("5").get("tool-C").get("data").get(2).get("userId").getIntValue()), 5);
                
                Assert.assertEquals(
                        (reconstructedUserData.get("5").get("tool-E").get("data").get(0).get("userId").getIntValue()), 5);
                Assert.assertEquals(
                        (reconstructedUserData.get("5").get("tool-E").get("data").get(1).get("userId").getIntValue()), 5);
                Assert.assertEquals(
                        (reconstructedUserData.get("5").get("tool-Y").get("data").get(0).get("userId").getIntValue()), 5);
                
        }

        @Test
        public void generateBoundingBoxCordinates_shouldGenrateBoxCordinates() throws IOException{
         String jsonData = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("toolData.json"));
         List<Map<String, String>> boxList =  MeasurementUtils.generateBoundingBoxCordinates(jsonData);
         Assert.assertEquals(boxList.size(), 2);
         Assert.assertEquals(boxList.get(0).size(), 8);
         Assert.assertEquals(boxList.get(1).size(), 8);
        }
        
}
