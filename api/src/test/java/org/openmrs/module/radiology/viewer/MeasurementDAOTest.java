/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer;

import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openmrs.User;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.module.radiology.viewer.event.MeasurementEventData;
import org.openmrs.test.BaseModuleContextSensitiveTest;
import org.springframework.beans.factory.annotation.Autowired;

public class MeasurementDAOTest extends BaseModuleContextSensitiveTest {
	
	@Autowired
	private MeasurementDAO dao;
	
	private UserService userService;
	
	String toolData = "{\"tool-C\":{\"data\":[{\"dataC\":\"xx\",\"userId\": 2},{\"dataC\":\"xx\"}]}}";
	
	private final String studyInstanceUid = "1.2.826.0.1.3680043.8.1055.1.20111103111148288.98361414.79379639";
	
	private final String seriesInstanceUid = "1.2.826.0.1.3680043.8.1055.1.20111103111153403.39691445.89792679";
	
	private final String sopInstanceUid = "1.2.826.0.1.3680043.8.1055.1.20111103111156215.14436053.35031727";
	
	@Before
	public void init() throws Exception {
		executeDataSet("org/openmrs/module/radiology/include/MeasurementTestDataset.xml");
		userService = Context.getUserService();
	}
	
	@Test
	public void saveMeasurement_shouldcreateNewOrUpdateMeasurement() throws Exception {
		final String newSopInstanceUid = "1.2.826.0.1.3680043.8.1055.1.20111103111156215.14436053.350317xx";
		
		Measurement measurement = new Measurement();
		measurement.setStudyInstanceUid(studyInstanceUid);
		measurement.setSeriesInstanceUid(seriesInstanceUid);
		measurement.setSopInstanceUid(newSopInstanceUid);
		User user5 = userService.getUser(5);
		measurement.getMeasurementData().put(user5, toolData);
		
		Assert.assertNull(dao.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, newSopInstanceUid));
		Measurement newMeasurement = dao.saveMeasurement(measurement);
		// AFter saving
		Assert.assertNotNull(dao.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, newSopInstanceUid));
		Assert.assertEquals(newMeasurement.getMeasurementData().get(user5), toolData);
		Assert.assertNotNull(newMeasurement.getId());
	}
	
	@Test
	public void getMeasurementByImage_shouldGetMeasurementByImage() throws Exception {
		Measurement measurement = dao.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, sopInstanceUid);
		Assert.assertEquals(measurement.getMeasurementId(), Integer.valueOf(1));
	}
	
	@Test
	public void deleteMeasurement_shouldDeleteMeasurement() throws Exception {
		Measurement measurement = dao.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, sopInstanceUid);
		Assert.assertNotNull(measurement);
		dao.deleteMeasurement(measurement);
		measurement = dao.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, sopInstanceUid);
		Assert.assertNull(measurement);
	}
	
	@Test
	public void saveMeasurementEventData_shouldSaveMeasurementEventData() throws Exception {
		MeasurementEventData measurementEventData = new MeasurementEventData();
		measurementEventData.setSeriesInstanceUid(seriesInstanceUid);
		measurementEventData.setSopInstanceUid(sopInstanceUid);
		measurementEventData.setStudyInstanceUid(studyInstanceUid);
		measurementEventData.setData(toolData);
		//before Save
		Assert.assertNull(measurementEventData.getId());
		//after Save
		MeasurementEventData newMeasurementEventData = dao.saveMeasurementEventData(measurementEventData);
		Assert.assertNotNull(newMeasurementEventData.getId());
		Assert.assertEquals(newMeasurementEventData.getSeriesInstanceUid(), seriesInstanceUid);
		Assert.assertEquals(newMeasurementEventData.getSopInstanceUid(), sopInstanceUid);
		Assert.assertEquals(newMeasurementEventData.getStudyInstanceUid(), studyInstanceUid);
	}
	
	@Test
	public void getAllMeasurementEventData_shouldReturnAllMeasurementEventDataInOrderOfDateCreated() throws Exception {
		List<MeasurementEventData> allEventData = dao.getAllMeasurementEventData();
		Assert.assertTrue(allEventData.get(0).getDateCreated().compareTo(allEventData.get(1).getDateCreated()) < 0);
	}
	
	@Test
	public void deleteMeasurementEventData_shouldDeleteMeasurementEventData() throws Exception {
		List<MeasurementEventData> allEventData = dao.getAllMeasurementEventData();
		dao.deleteMeasurementEventData(allEventData.get(0));
		Assert.assertEquals(dao.getAllMeasurementEventData().size(), allEventData.size() - 1);
	}
	
}
