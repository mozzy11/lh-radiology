/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module;

import org.openmrs.module.radiology.viewer.event.MeasurementEventListener;

public class TestDaemonToken {
	
	private class Activator extends BaseModuleActivator implements DaemonTokenAware {
	
		@Override
		public void setDaemonToken(DaemonToken token) {
			MeasurementEventListener.setDaemonToken(token);
		}
		
	}
	
	/**
	 * Sets a valid Daemon token to the Module in test
	 */
	public void setDaemonToken() {
		Module module = new Module("Rdiology Test Module");
		module.setModuleId("Radiology");
		final Activator activator = new Activator();
		module.setModuleActivator(activator);
		ModuleFactory.passDaemonToken(module);
	}
}
