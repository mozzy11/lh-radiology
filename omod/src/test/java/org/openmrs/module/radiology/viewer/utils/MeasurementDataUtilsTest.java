/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer.utils;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.openmrs.module.radiology.viewer.rest.MeasurementDataConstants;
import org.openmrs.module.radiology.viewer.rest.utils.MeasurementDataUtils;

public class MeasurementDataUtilsTest {
        
        @Test
        public void generateUidsUtil_shouldGenerateImageUids() {
                
                String imageId = "wadors:http://localhost:8899/dicom-web/studies/1.2.826.0.1.3680043.8.1055.1.20111103111148288.98361414.79379639/series/1.2.826.0.1.3680043.8.1055.1.20111103111153403.39691445.89792679/instances/1.2.826.0.1.3680043.8.1055.1.20111103111156215.14436053.35031727/frames/1";
                
                Map<String, String> uidMap = MeasurementDataUtils.generateUids(imageId);
                
                Assert.assertEquals(uidMap.get(MeasurementDataConstants.STUDY_FIELD_NAME),
                        "1.2.826.0.1.3680043.8.1055.1.20111103111148288.98361414.79379639");
                Assert.assertEquals(uidMap.get(MeasurementDataConstants.SERIES_FIELD_NAME),
                        "1.2.826.0.1.3680043.8.1055.1.20111103111153403.39691445.89792679");
                Assert.assertEquals(uidMap.get(MeasurementDataConstants.SOP_FIELD_NAME),
                        "1.2.826.0.1.3680043.8.1055.1.20111103111156215.14436053.35031727");
        }
        
}
