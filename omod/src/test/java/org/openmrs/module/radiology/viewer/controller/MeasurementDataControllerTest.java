/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

import org.aopalliance.aop.Advice;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.junit.Before;
import org.junit.Assert;
import org.junit.Test;
import org.openmrs.User;
import org.openmrs.api.APIException;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.api.context.UserContext;
import org.openmrs.module.AdvicePoint;
import org.openmrs.module.TestDaemonToken;
import org.openmrs.module.radiology.viewer.Measurement;
import org.openmrs.module.radiology.viewer.MeasurementService;
import org.openmrs.module.radiology.viewer.aop.MeasurementEventAop;
import org.openmrs.module.radiology.viewer.event.MeasurementEventData;
import org.openmrs.module.webservices.rest.web.v1_0.controller.MainResourceControllerTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.bind.annotation.RequestMethod;

public class MeasurementDataControllerTest extends MainResourceControllerTest {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private MeasurementService measureService;
    
    ObjectMapper mapper = new ObjectMapper();
    
    String imageId = "wadors:http://localhost:8899/dicom-web/studies/1.2.826.0.1.3680043.8.1055.1.20111103111148288.98361414.79379639/series/1.2.826.0.1.3680043.8.1055.1.20111103111153403.39691445.89792679/instances/1.2.826.0.1.3680043.8.1055.1.20111103111156215.14436053.35031727/frames/1";
    
    String newImageId = "wadors:http://localhost:8899/dicom-web/studies/1.2.826.0.1.3680043.8.1055.1.20111103111148288.98361414.79379639/series/1.2.826.0.1.3680043.8.1055.1.20111103111153403.39691445.89792679/instances/1.2.826.0.1.3680043.8.1055.1.20111103111156215.14436053.35031new/frames/1";
    
    private final String studyInstanceUid = "1.2.826.0.1.3680043.8.1055.1.20111103111148288.98361414.79379639";
    
    private final String seriesInstanceUid = "1.2.826.0.1.3680043.8.1055.1.20111103111153403.39691445.89792679";
    
    private final String sopInstanceUid = "1.2.826.0.1.3680043.8.1055.1.20111103111156215.14436053.35031727";
    
    private final String newSopInstanceUid = "1.2.826.0.1.3680043.8.1055.1.20111103111156215.14436053.35031new";
    
    private final String toolData = "{\"tool-C\":{\"data\":[{\"dataC\":\"xx\"},{\"dataC\":\"xx\"}]}}";
    
    private User user2;
    
    private User user3;
    
    @Before
    public void init() throws Exception {
        executeDataSet("MeasurementTestDataset.xml");
        user2 = userService.getUser(2);
        user3 = userService.getUser(3);
        new TestDaemonToken().setDaemonToken();
        Advice advice = (Advice) (new AdvicePoint(MeasurementService.class.getCanonicalName(),
                Context.loadClass(MeasurementEventAop.class.getCanonicalName()))).getClassInstance();
        Context.addAdvice(Context.loadClass(MeasurementService.class.getCanonicalName()), advice);
    }
    
    @Override
    public long getAllCount() {
        return 0;
    }
    
    @Override
    public String getURI() {
        return "measurement";
    }
    
    @Override
    public String getUuid() {
        return null;
    }
    
    @Test
    public void getMeasureData_shouldGetMeasurementData() throws Exception {
        String base64encodedImageId = Base64.getEncoder().encodeToString(imageId.getBytes());
        String urlEncodedImageId = URLEncoder.encode(base64encodedImageId, StandardCharsets.UTF_8.name());
        
        MockHttpServletRequest req = request(RequestMethod.GET, getURI() + "/get/" + urlEncodedImageId);
        MockHttpServletResponse response = handle(req);
        JsonNode resp = mapper.readTree(response.getContentAsString());
        assertTrue(resp.has(imageId));
        assertTrue(resp.get(imageId).has("tool-A"));
        assertEquals(response.getStatus(), 200);
    }
    
    @Test
    public void saveMeasurementData_shouldSaveNewMeasurementData() throws Exception {
        String base64encodedImageId = Base64.getEncoder().encodeToString(newImageId.getBytes());
        String urlEncodedImageId = URLEncoder.encode(base64encodedImageId, StandardCharsets.UTF_8.name());
        
        JsonNode measurement = mapper.readTree(toolData);
        ObjectNode measuremetData = mapper.createObjectNode();
        measuremetData.put("imageId", newImageId);
        measuremetData.put("measureData", measurement.toString());
        String content = measuremetData.toString();
        
        MockHttpServletRequest fetch = request(RequestMethod.GET, getURI() + "/get/" + urlEncodedImageId);
        // fetch new measurement before saving it
        MockHttpServletResponse response = handle(fetch);
        JsonNode resp = mapper.readTree(response.getContentAsString());
        assertFalse(resp.get(newImageId).has("tool-C"));
        assertEquals(resp.get(newImageId).toString(), "{}");
        // save the new Measurement data
        MockHttpServletRequest req = request(RequestMethod.POST, getURI() + "/save");
        req.setContent(content.getBytes("UTF-8"));
        handle(req);
        // fetch the newly added Measurement data after saving
        response = handle(fetch);
        resp = mapper.readTree(response.getContentAsString());
        assertTrue(resp.has(newImageId));
        assertTrue(resp.get(newImageId).has("tool-C"));
        assertEquals(response.getStatus(), 200);
    }
    
    @Test
    public void saveMeasurementData_shouldAddNewUserDataToExistingMeasurement() throws Exception {
        User newUser = userService.getUser(5);
        UserContext usercontext = Context.getUserContext();
        usercontext.becomeUser(newUser.getSystemId());
        Context.setUserContext(usercontext);
        
        JsonNode measurement = mapper.readTree(toolData);
        ObjectNode measuremetData = mapper.createObjectNode();
        measuremetData.put("imageId", imageId);
        measuremetData.put("measureData", measurement.toString());
        String content = measuremetData.toString();
        
        JsonNode measurementData = mapper.readTree(
            measureService.getMeasurementDataByImageAndUser(studyInstanceUid, seriesInstanceUid, sopInstanceUid, newUser));
        assertNotNull(measurementData);
        Assert.assertFalse(measurementData.has("tool-C"));
        
        MockHttpServletRequest req = request(RequestMethod.POST, getURI() + "/save");
        req.setContent(content.getBytes("UTF-8"));
        handle(req);
        
        measurementData = mapper.readTree(
            measureService.getMeasurementDataByImageAndUser(studyInstanceUid, seriesInstanceUid, sopInstanceUid, newUser));
        
        Assert.assertTrue(measurementData.has("tool-C"));
    }
    
    @Test
    public void saveMeasurementData_shouldUpdateExistingUserData() throws Exception {
        UserContext usercontext = Context.getUserContext();
        usercontext.becomeUser(user2.getSystemId());
        Context.setUserContext(usercontext);
        
        JsonNode measurement = mapper.readTree(toolData);
        ObjectNode measuremetData = mapper.createObjectNode();
        measuremetData.put("imageId", imageId);
        measuremetData.put("measureData", measurement.toString());
        String content = measuremetData.toString();
        
        JsonNode measurementData = mapper.readTree(
            measureService.getMeasurementDataByImageAndUser(studyInstanceUid, seriesInstanceUid, sopInstanceUid, user2));
        // before updating
        Assert.assertTrue(measurementData.has("tool-A"));
        Assert.assertFalse(measurementData.has("tool-C"));
        
        MockHttpServletRequest req = request(RequestMethod.POST, getURI() + "/save");
        req.setContent(content.getBytes("UTF-8"));
        handle(req);
        
        JsonNode updatedMeasurementData = mapper.readTree(
            measureService.getMeasurementDataByImageAndUser(studyInstanceUid, seriesInstanceUid, sopInstanceUid, user2));
        // after updating
        Assert.assertTrue(updatedMeasurementData.has("tool-C"));
        Assert.assertFalse(updatedMeasurementData.has("tool-A"));
    }
    
    @Test
    public void saveMeasurementData_shouldRemoveExistingMeasurementInstanceWhenUserDataIsEmpty() throws Exception {
        UserContext usercontext = Context.getUserContext();
        usercontext.becomeUser(user2.getSystemId());
        Context.setUserContext(usercontext);
        
        JsonNode measurement = mapper.readTree("{}");
        ObjectNode measuremetData = mapper.createObjectNode();
        measuremetData.put("imageId", imageId);
        measuremetData.put("measureData", measurement.toString());
        String content = measuremetData.toString();
        
        Measurement existingMeasurement = measureService.getMeasurementByImage(studyInstanceUid, seriesInstanceUid,
            sopInstanceUid);
        Assert.assertNotNull(existingMeasurement);
        
        MockHttpServletRequest req = request(RequestMethod.POST, getURI() + "/save");
        req.setContent(content.getBytes("UTF-8"));
        handle(req);
        // after 
        existingMeasurement = measureService.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, sopInstanceUid);
        Assert.assertNull(existingMeasurement);
    }
    
    @Test
    public void saveMeasurementData_shouldProperlySaveOtherUsersDataWhenAdminSaves() throws Exception {
        final String adminMergedData = "{\"tool-C\":{\"data\":[{\"dataC\":\"xx\",\"userId\": 2}]},\"tool-E\":{\"data\":[{\"dataC\":\"xx_d\",\"userId\": 3},{\"dataC\":\"xx2\",\"userId\": null}]} ,\"tool-D\":{\"data\":[{\"dataC\":\"xx\",\"userId\": 4}]}}";
        
        User user_admin = userService.getUser(4);
        UserContext usercontext = Context.getUserContext();
        usercontext.becomeUser(user_admin.getSystemId());
        Context.setUserContext(usercontext);
        
        Measurement newMeasurement = measureService.getMeasurementByImage(studyInstanceUid, seriesInstanceUid,
            newSopInstanceUid);
        Assert.assertNull(newMeasurement);
        
        JsonNode measurement = mapper.readTree(adminMergedData);
        ObjectNode measuremetData = mapper.createObjectNode();
        measuremetData.put("imageId", newImageId);
        measuremetData.put("measureData", measurement.toString());
        String content = measuremetData.toString();
        
        MockHttpServletRequest req = request(RequestMethod.POST, getURI() + "/save");
        req.setContent(content.getBytes("UTF-8"));
        handle(req);
        // after saving new MeasuremntData
        newMeasurement = measureService.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, newSopInstanceUid);
        Assert.assertNotNull(newMeasurement);
        
        // should save data coresponding to other users
        JsonNode user2Data = mapper.readTree(newMeasurement.getMeasurementData().get(user2));
        Assert.assertTrue(user2Data.has("tool-C"));
        Assert.assertFalse(user2Data.has("tool-E"));
        Assert.assertFalse(user2Data.has("tool-D"));
        
        JsonNode user3Data = mapper.readTree(newMeasurement.getMeasurementData().get(user3));
        Assert.assertTrue(user3Data.has("tool-E"));
        Assert.assertFalse(user3Data.has("tool-c"));
        Assert.assertFalse(user3Data.has("tool-D"));
        
        // should also save admin specific data
        JsonNode userAdminData = mapper.readTree(newMeasurement.getMeasurementData().get(user_admin));
        Assert.assertFalse(userAdminData.has("tool-C"));
        Assert.assertTrue(userAdminData.has("tool-E"));
        Assert.assertTrue(userAdminData.has("tool-D"));
    }
    
    @Test
    public void saveMeasurementData_shouldSaveNewMeasurementEventData() throws Exception {
        // Before saving
        List<MeasurementEventData> allEventData = measureService.getAllMeasurementEventData();
        JsonNode measurement = mapper.readTree(toolData);
        ObjectNode measuremetData = mapper.createObjectNode();
        measuremetData.put("imageId", newImageId);
        measuremetData.put("measureData", measurement.toString());
        String content = measuremetData.toString();
        
        MockHttpServletRequest req = request(RequestMethod.POST, getURI() + "/save");
        req.setContent(content.getBytes("UTF-8"));
        handle(req);
        //should publish a measurement event and save new Measurement event data
        // pause the current thread to wait for the Event Publisher thread
        Thread.sleep(1000);
        Assert.assertEquals(measureService.getAllMeasurementEventData().size(), allEventData.size() + 3);
    }
    
    @Test
    public void saveMeasurementData_shouldNotSaveNewMeasurementEventDataWhenDataIsEmpty() throws Exception {
        // Before saving
        List<MeasurementEventData> allEventData = measureService.getAllMeasurementEventData();
        
        JsonNode measurement = mapper.readTree("{}");
        ObjectNode measuremetData = mapper.createObjectNode();
        measuremetData.put("imageId", newImageId);
        measuremetData.put("measureData", measurement.toString());
        String content = measuremetData.toString();
        
        MockHttpServletRequest req = request(RequestMethod.POST, getURI() + "/save");
        req.setContent(content.getBytes("UTF-8"));
        handle(req);
        // should publish a measurement event and save new Measurement event data
        Thread.sleep(1000);// pause the current thread to wait for the Event Publisher thread
        Assert.assertEquals(measureService.getAllMeasurementEventData().size(), allEventData.size());
    }
    
    @Test
    public void saveMeasurementData_shouldNotSaveNewMeasurementEventDataWhenDataContainsNoAnnotationData() throws Exception {
        // Before saving
        List<MeasurementEventData> allEventData = measureService.getAllMeasurementEventData();
        
        JsonNode measurement = mapper.readTree("{\"tool\":{\"data\":[]}}");
        ObjectNode measuremetData = mapper.createObjectNode();
        measuremetData.put("imageId", newImageId);
        measuremetData.put("measureData", measurement.toString());
        String content = measuremetData.toString();
        
        MockHttpServletRequest req = request(RequestMethod.POST, getURI() + "/save");
        req.setContent(content.getBytes("UTF-8"));
        handle(req);
        // should publish a measurement event and save new Measurement event data
        Thread.sleep(1000);// pause the current thread to wait for the Event Publisher thread
        Assert.assertEquals(measureService.getAllMeasurementEventData().size(), allEventData.size());
    }
    
    @Override
    @Test(expected = APIException.class)
    public void shouldGetAll() throws Exception {
        super.shouldGetAll();
    }
    
    @Override
    @Test(expected = APIException.class)
    public void shouldGetDefaultByUuid() throws Exception {
        super.shouldGetDefaultByUuid();
    }
    
    @Override
    @Test(expected = APIException.class)
    public void shouldGetFullByUuid() throws Exception {
        super.shouldGetFullByUuid();
    }
    
    @Override
    @Test(expected = APIException.class)
    public void shouldGetRefByUuid() throws Exception {
        super.shouldGetRefByUuid();
    }
    
}
