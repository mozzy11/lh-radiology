window.config = {
    routerBasename: openmrsContextPath + '/module/radiology/ohif.htm',
    servers: {
        dicomWeb: [
            {
                name: serverName,
                wadoUriRoot: wadoUriRoot,
                qidoRoot: qidoRoot,
                wadoRoot: wadoRoot,
                qidoSupportsIncludeField: true,
                imageRendering: imageRendering,
                thumbnailRendering: thumbnailRendering,
                requestOptions: {
                    requestFromBrowser: true,
                    auth: auth,
                    "logRequests": true,
                    "logResponses": true,
                    "logTiming": true
                },
            },
        ],
    },
};

var containerId = "root";
var componentRenderedOrUpdatedCallback = function () {
    console.log('OHIF Viewer rendered/updated');
}
window.OHIFViewer.installViewer(window.config, containerId, componentRenderedOrUpdatedCallback);

function save() {
    const toolData = cornerstoneTools.globalImageIdSpecificToolStateManager.saveToolState();
    for (data in toolData) {
        const measureData = { "imageId": data, "measureData": JSON.stringify(toolData[data]) };
        fetch(openmrsContextPath + '/ws/rest/v1/measurement/save', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(measureData),
        })
            .then((response) => response.status)
            .then((status) => {
                ohif.app.servicesManager.services.UINotificationService.show({ message: succesMsg, duration: 4000 })
            })

    }
}

var measurementChanged = false;
var encodedUrl = "";
globalActiveFunction = function (element) {
    save();
}

var $j = jQuery.noConflict();

$j(document).ready(function () {
    $j("#loading").hide();
});

function displayLoading() {
    $j("#loading").show();  
}

// hiding loading 
function hideLoading() {
    $j("#loading").hide();
}

cornerstone.events.addEventListener(cornerstone.EVENTS.ELEMENT_ENABLED, function (event) {
    event.detail.element.addEventListener(cornerstone.EVENTS.NEW_IMAGE, function (event) {
       
        displayLoading()
        const imageId = event.detail.image.imageId;
        const encodedImageId = btoa(imageId);
        encodedUrl = encodeURI(encodedImageId);
        fetch(openmrsContextPath + '/ws/rest/v1/measurement/get/' + encodedUrl,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            }
        )
            .then(response => response.json())
            .then(data => {
                hideLoading();
                for (imageUrl in data) {
                    var imageData = data[imageUrl]
                    if (imageUrl == imageId) {
                        for (toolName in imageData) {
                            var toolData = imageData[toolName]
                            var toolDataArray = toolData["data"]
                            for (var i = 0; i < toolDataArray.length; i++) {
                                var measurements = toolDataArray[i];
                                var userId = measurements["userId"];
                                cornerstoneTools.addToolState(event.detail.element, toolName, measurements);
                                var newData = cornerstoneTools.getToolState(event.detail.element, toolName)["data"][i];
                                newData["userId"] = userId;
                            }
                        }
                    }
                }

            })
            .catch((error) => {
                hideLoading();
                alert('Error:', error);
            });
    });

    event.detail.element.addEventListener(cornerstoneTools.EVENTS.MEASUREMENT_COMPLETED, function (event) {
        measurementChanged = true;
    });

    event.detail.element.addEventListener(cornerstoneTools.EVENTS.MEASUREMENT_REMOVED, function (event) {
        measurementChanged = true;

        fetch(openmrsContextPath + '/ws/rest/v1/measurement/get/' + encodedUrl,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            }
        )
            .then(response => response.json())
            .then(originalData => {
                var data = cornerstoneTools.globalImageIdSpecificToolStateManager.saveToolState()
                for (imageUrl in data) {
                    var imageData = data[imageUrl]
                    for (toolName in imageData) {
                        var toolData = imageData[toolName]
                        var toolDataAray = toolData["data"]
                        for (var i = 0; i < toolDataAray.length; i++) {
                            var measurements = toolDataAray[i];
                            var originalDataArray = originalData[imageUrl][toolName]["data"]
                            for (var x = 0; x < originalDataArray.length; x++) {
                                var originalMeasurements = originalDataArray[x]
                                if (originalMeasurements["uuid"] == measurements["uuid"]) {
                                    measurements["userId"] = originalMeasurements["userId"]
                                }

                            }
                        }
                    }

                }

            })
    });
    event.detail.element.addEventListener(cornerstoneTools.EVENTS.MEASUREMENT_MODIFIED, function (event) {
        measurementChanged = true;
    });

    event.detail.element.addEventListener(cornerstone.EVENTS.ELEMENT_DISABLED, function (event) {
        if (measurementChanged) {
            if (confirm(saveMsg) == true) {
                save();
                measurementChanged = false;
            } 
        }
    });

    event.detail.element.addEventListener(cornerstoneTools.EVENTS.MEASUREMENT_ADDED, function(event){
        if (event.detail.toolType == 'ArrowAnnotate'){
            event.detail.measurementData.color = toolColor;
        }	
      });

});