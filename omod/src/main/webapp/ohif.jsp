    <%@ include file="/WEB-INF/template/include.jsp" %>
	<%@ include file="/WEB-INF/template/header.jsp" %>
	   
		<openmrs:hasPrivilege privilege="View Orders">	
			<div id="root" style="width:100%; height:600px;"></div>
			<div id="loading"></div>
		</openmrs:hasPrivilege>
        <openmrs:message code="radiology.measurements.save.succes" var = "measurementSuccesMsg"/>
		<openmrs:message code="radiology.measurements.save" var = "saveMeasurementMsg"/>
		<openmrs:htmlInclude file="/moduleResources/radiology/js/index.umd.js" />
		<openmrs:globalProperty key="radiology.ohifDicomServerName" var="dicomServerName" />
		<openmrs:globalProperty key="radiology.ohifWadoUriRoot" var="wadoUriRoot" />
		<openmrs:globalProperty key="radiology.ohifQidoRoot" var="qidoRoot" />
		<openmrs:globalProperty key="radiology.ohifWadoRoot" var="wadoRoot" />
		<openmrs:globalProperty key="radiology.ohifImageRendering" var="imageRendering" />
		<openmrs:globalProperty key="radiology.ohifThumbnailRendering" var="thumbnailRendering" />
		<openmrs:globalProperty key="radiology.ohifDicomServerUser" var="dicomServerUser" />
		<openmrs:globalProperty key="radiology.ohifDicomServerPassword" var="dicomServerPassword" />
		<openmrs:globalProperty key="radiology.ohifArrowToolColor" var="toolColor" />

		<script>
			var succesMsg = '${measurementSuccesMsg}';
			var saveMsg = '${saveMeasurementMsg}';
			var serverName = '${dicomServerName}';
            var wadoUriRoot = '${wadoUriRoot}';
            var qidoRoot = '${qidoRoot}' ;
            var wadoRoot = '${wadoRoot}';
            var imageRendering = '${imageRendering}';
            var thumbnailRendering = '${thumbnailRendering}';
            var auth = '${dicomServerUser}:${dicomServerPassword}';
			var toolColor = '${toolColor}';
		</script>
		<openmrs:htmlInclude file="/moduleResources/radiology/css/loader.css" />
		<openmrs:htmlInclude file="/moduleResources/radiology/js/viewer.js" />		
		<%@ include file="/WEB-INF/template/footer.jsp" %>