/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer.rest.controller;

import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.openmrs.User;
import org.openmrs.api.context.Context;
import org.openmrs.module.radiology.RadiologyPrivileges;
import org.openmrs.module.radiology.viewer.Measurement;
import org.openmrs.module.radiology.viewer.MeasurementService;
import org.openmrs.module.radiology.viewer.rest.MeasurementDataConstants;
import org.openmrs.module.radiology.viewer.rest.utils.MeasurementDataUtils;
import org.openmrs.module.radiology.viewer.utils.MeasurementUtils;
import org.openmrs.module.webservices.rest.web.RestConstants;
import org.openmrs.module.webservices.rest.web.v1_0.controller.BaseRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping(value = "/rest/" + RestConstants.VERSION_1 + "/measurement")
public class MeasurementDataController extends BaseRestController {
    
    @Autowired
    MeasurementService measureService;
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public void saveMeasurementData(@RequestBody Map<String, String> body) {
        String imageId = body.get("imageId");
        String measureData = body.get("measureData");
        Map<String, String> uidMap = MeasurementDataUtils.generateUids(imageId);
        String studyInstanceUid = uidMap.get(MeasurementDataConstants.STUDY_FIELD_NAME);
        String seriesInstanceUid = uidMap.get(MeasurementDataConstants.SERIES_FIELD_NAME);
        String sopInstanceUid = uidMap.get(MeasurementDataConstants.SOP_FIELD_NAME);
        User user = Context.getAuthenticatedUser();
        
        String updatedUserData = MeasurementUtils.addUserIdIntoUserData(user, measureData);
        JsonNode constructedUsersData = MeasurementUtils.constructUserData(updatedUserData);
        List<String> userIdlist = new ArrayList<String>();
        Iterator<String> fieldNames = constructedUsersData.getFieldNames();
        fieldNames.forEachRemaining(userIdlist::add);
        Map<User, String> measurementData = new HashMap<User, String>();
        for (String userId : userIdlist) {
            String constructedUserData = constructedUsersData.get(userId).toString();
            User constructedUser = Context.getUserService().getUser(Integer.valueOf(userId));
            measurementData.put(constructedUser, constructedUserData);
        }
        createOrUpdateMeasurement(studyInstanceUid, seriesInstanceUid, sopInstanceUid, measurementData);
        
    }
    
    @RequestMapping(value = "/get/{imageId}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public String getMeasurementData(@PathVariable("imageId") String encodedimageId) throws IOException {
        String urlDecodedImageId = URLDecoder.decode(encodedimageId, StandardCharsets.UTF_8.name());
        String base64Decoded = new String(Base64.getDecoder().decode(urlDecodedImageId));
        Map<String, String> uidMap = MeasurementDataUtils.generateUids(base64Decoded);
        
        String studyInstanceUid = uidMap.get(MeasurementDataConstants.STUDY_FIELD_NAME);
        String seriesInstanceUid = uidMap.get(MeasurementDataConstants.SERIES_FIELD_NAME);
        String sopInstanceUid = uidMap.get(MeasurementDataConstants.SOP_FIELD_NAME);
        User user = Context.getAuthenticatedUser();
        
        String toolData = measureService.getMeasurementDataByImageAndUser(studyInstanceUid, seriesInstanceUid,
            sopInstanceUid, user);
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode measuremetData = mapper.createObjectNode();
        
        JsonNode data = mapper.readTree(toolData);
        measuremetData.put(base64Decoded, data);
        return measuremetData.toString();
    }
    
    private void createOrUpdateMeasurement(String studyInstanceUid, String seriesInstanceUid, String sopInstanceUid,
            Map<User, String> measurementData) {
        Measurement measurement = measureService.getMeasurementByImage(studyInstanceUid, seriesInstanceUid, sopInstanceUid);
        if (!measurementData.isEmpty()) {
            if (measurement != null) {
                if (Context.hasPrivilege(RadiologyPrivileges.RADIOLOGY_ADMIN)) {
                    measurement.getMeasurementData().clear();
                }
                for (Map.Entry<User, String> entry : measurementData.entrySet()) {
                    measurement.getMeasurementData().put(entry.getKey(), entry.getValue());
                }
            } else {
                measurement = new Measurement();
                measurement.setStudyInstanceUid(studyInstanceUid);
                measurement.setSeriesInstanceUid(seriesInstanceUid);
                measurement.setSopInstanceUid(sopInstanceUid);
                for (Map.Entry<User, String> entry : measurementData.entrySet()) {
                    measurement.getMeasurementData().put(entry.getKey(), entry.getValue());
                }
            }
            measureService.saveMeasurement(measurement);
        } else {
            if (measurement != null) {
                measureService.purgeMeasurement(measurement);
            }
        }
    }
}
