/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer.rest.utils;

import java.util.HashMap;
import java.util.Map;

import org.openmrs.module.radiology.viewer.rest.MeasurementDataConstants;

public class MeasurementDataUtils {
    
    /**
     * Generates Imgage UIDs from the imageId/imageUrl
     *
     * @param imageId imgageId of the Image
     * @return uidMap Map of the extracted UIDs
     */
    public static Map<String, String> generateUids(String imageId) {
        
        final String studies = "/studies/";
        final String series = "/series/";
        final String instances = "/instances/";
        
        String studyInstanceUid = imageId.split(studies)[1].split("/")[0];
        String seriesInstanceUid = imageId.split(series)[1].split("/")[0];
        String sopInstanceUid = imageId.split(instances)[1].split("/")[0];
        Map<String, String> uidMap = new HashMap<String, String>();
        
        uidMap.put(MeasurementDataConstants.STUDY_FIELD_NAME, studyInstanceUid);
        uidMap.put(MeasurementDataConstants.SERIES_FIELD_NAME, seriesInstanceUid);
        uidMap.put(MeasurementDataConstants.SOP_FIELD_NAME, sopInstanceUid);
        return uidMap;
    }
}
