/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.viewer.rest;

public class MeasurementDataConstants {

     /**
     * Study Field Name
     */
    public static final String STUDY_FIELD_NAME = "studyInstanceUid";

    /**
     * Series Field Name
     */
    public static final String SERIES_FIELD_NAME = "seriesInstanceUid";

    /**
     * SOP Instance Field Name
     */
    public static final String SOP_FIELD_NAME = "sopInstanceUid";;

    private MeasurementDataConstants() {
    }
    
}
